summary: Simple kernel sanity check that loads and unloads various kernel modules
description: |+
    This test is designed to test basic kernel functionality.
    It loads some kernel modules from a list of modules in a file and
    checks to see if they have loaded and can be removed. Architecture
    specific modules may be used by adding additional files to this
    test in the form of modules.$arch where $arch is the name of the
    architecture the test is running on. If there is no architecture
    specific file the test will use the modules listed in modules.default.
    For any RHEL release specific modules that don't match between releases
    create modules.rhel[345] file with release-specific modules listed in
    it. These modules will be appended to default or arch-specific module
    list.

enabled: true
id: 6f4b3c26-4c60-4e7d-8cdc-8a7de9a1da8e
tag:
  - KT1-9
  - fmf-export
  - KernelTier1
contact: Jeff Bastian <jbastian@redhat.com>
component:
  - kernel
path: /misc/module-load
test: bash ./runtest.sh
framework: shell
require:
  - beakerlib
  - coreutils
  - wget
  - git
  - time
  - patch
  - bzip2
  - autoconf
  - glib2-devel
  - make
  - gettext
  - automake
  - gcc
  - libtool
  - bison
  - flex
recommend:
  - kernel-utils
duration: 75m
extra-summary: /misc/module-load
extra-task: /misc/module-load
