From 57ab2160c0b002cbeeb1cba477cf8875ca9d660d Mon Sep 17 00:00:00 2001
From: David Hildenbrand <david@redhat.com>
Date: Tue, 8 Oct 2024 15:59:33 +0200
Subject: [PATCH] move_pages04: remove special-casing for kernels < 4.3

Let's stop pretending that kernels < 4.3 did the right thing: they
didn't. Let the test fail if we run on these kernels.

Suggested-by: Jan Stancek <jstancek@redhat.com>
Signed-off-by: David Hildenbrand <david@redhat.com>
Reviewed-by: Jan Stancek <jstancek@redhat.com>
Reviewed-by: Cyril Hrubis <chrubis@suse.cz>
---
 .../kernel/syscalls/move_pages/move_pages04.c      | 14 ++++----------
 1 file changed, 4 insertions(+), 10 deletions(-)

diff --git a/testcases/kernel/syscalls/move_pages/move_pages04.c b/testcases/kernel/syscalls/move_pages/move_pages04.c
index 03a91e223..4a541eb1b 100644
--- a/testcases/kernel/syscalls/move_pages/move_pages04.c
+++ b/testcases/kernel/syscalls/move_pages/move_pages04.c
@@ -40,8 +40,7 @@
  *         -ENOENT. Note that kernels >= 4.3 [1] and < 6.12 [2] wrongly returned
  *         -EFAULT by accident.
  *      3. Check if the corresponding status for "shared zero page" is set to:
- *         -ENOENT for kernels < 4.3
- *         -EFAULT for kernels >= 4.3 [1]
+ *         -EFAULT. Note that kernels < 4.3 [1] wrongly returned -ENOENT.
  *      4. Check if the corresponding status for "invalid memory area" is set
  *         to -EFAULT.
  *
@@ -102,12 +101,7 @@ int main(int argc, char **argv)
 	int lc;
 	unsigned int from_node;
 	unsigned int to_node;
-	int ret, exp_zero_page_status;
-
-	if ((tst_kvercmp(4, 3, 0)) >= 0)
-		exp_zero_page_status = -EFAULT;
-	else
-		exp_zero_page_status = -ENOENT;
+	int ret;
 
 	ret = get_allowed_nodes(NH_MEMS, 2, &from_node, &to_node);
 	if (ret < 0)
@@ -189,14 +183,14 @@ int main(int argc, char **argv)
 				tst_strerrno(ENOENT));
 		}
 
-		if (status[ZERO_PAGE] == exp_zero_page_status) {
+		if (status[ZERO_PAGE] == -EFAULT) {
 			tst_resm(TPASS, "status[%d] has expected value",
 				 ZERO_PAGE);
 		} else {
 			tst_resm(TFAIL, "status[%d] is %s, expected %s",
 				ZERO_PAGE,
 				tst_strerrno(-status[ZERO_PAGE]),
-				tst_strerrno(-exp_zero_page_status));
+				tst_strerrno(EFAULT));
 		}
 
 		if (status[INVALID_PAGE] == -EFAULT) {
-- 
2.47.0

